#ifndef SJPLANT_H
#define SJPLANT_H

#include <QQuickItem>

class Plant : public QQuickItem
{
    Q_OBJECT
    //For Heater in Boiler
    Q_PROPERTY(bool heaterState READ heaterState WRITE setHeaterState NOTIFY heaterStateChanged)
    Q_PROPERTY(float boilerPressure READ boilerPressure NOTIFY boilerPressureChanged)
    Q_PROPERTY(int boilerLimiter READ boilerLimiter WRITE setBoilerLimiter NOTIFY boilerLimiterChanged)
    Q_PROPERTY(int swBoiler READ swBoiler WRITE setSwBoiler NOTIFY swBoilerChanged)
    //For Suction pump
    Q_PROPERTY(bool pumpState READ pumpState WRITE setPumpState NOTIFY pumpStateChanged)
    Q_PROPERTY(float suctionPressure READ suctionPressure NOTIFY suctionPressureChanged)
    Q_PROPERTY(int suctionLimiter READ suctionLimiter WRITE setSuctionLimiter NOTIFY suctionLimiterChanged)
    Q_PROPERTY(int swPump READ swPump WRITE setSwPump NOTIFY swPumpChanged)
    //Other Pressures Measured
    Q_PROPERTY(float steamBufferPressure READ steamBufferPressure NOTIFY steamBufferPressureChanged)
    Q_PROPERTY(float vaccuumTankPressure READ vaccuumTankPressure NOTIFY vaccuumTankPressureChanged)

private:
    //For Heater in Boiler
    bool _heaterState;
    float _boilerPressure = 20;
    int _boilerLimiter;
    int _swBoiler;
    bool _flag_heated = false;
    //For Suction pump
    bool _pumpState;
    float _suctionPressure = -30;
    int _suctionLimiter;
    int _swPump;
    bool _flag_pumped = false;
    //Other pressures measured
    float _steamBufferPressure = 40;
    float _vaccuumTankPressure = -50;
    //Interlock flag
    int _interlock = 1;
    //for dummy module
    bool _flag_dummyFirstLoad = true;

    //low-water detection
    int _counter_lowwater = 0;
    float _prevPressure = 0;
    bool _flag_lowwater = false;

    //gpio digital input array
    int gpioDin;

public:
    explicit Plant(QQuickItem *parent = 0);

//For Heater in Boiler
    bool heaterState();
    void setHeaterState(bool p);
    float boilerPressure();
    Q_INVOKABLE int boilerLimiter();
    Q_INVOKABLE void setBoilerLimiter(int p);
    int swBoiler();
    Q_INVOKABLE void setSwBoiler(int p);
//For Suction pump
    bool pumpState();
    void setPumpState(bool p);
    float suctionPressure();
    Q_INVOKABLE int suctionLimiter();
    Q_INVOKABLE void setSuctionLimiter(int p);
    int swPump();
    Q_INVOKABLE void setSwPump(int p);

//Other pressure measured
    float steamBufferPressure();
    float vaccuumTankPressure();


//Automatic Control
    Q_INVOKABLE void autoControl(int var);

//Dummy module
    Q_INVOKABLE void dummyModule();

//Low-water Detection Logic
    Q_INVOKABLE int lowWaterCheck();
    Q_INVOKABLE void lowWaterFlagClear();
    Q_INVOKABLE void lowWaterDebug(int debugmode);

signals:
    //For Heater in Boiler
    void heaterStateChanged();
    void boilerPressureChanged();
    void boilerLimiterChanged();
    void swBoilerChanged();
    //For Suction pump
    void pumpStateChanged();
    void suctionPressureChanged();
    void suctionLimiterChanged();
    void swPumpChanged();
    //For other pressure transducers
    void steamBufferPressureChanged();
    void vaccuumTankPressureChanged();

public slots:
    void cppSlot(const QString &msg){
        qDebug() << "Called the C++ slot with message:" << msg;
    };
};


#endif // SJPLANT_H
