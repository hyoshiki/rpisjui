#include <iostream>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "sjplant.h" //Plant Model
#include "relayboard.h"
#include "adc.h"

class MyClass : public QObject
{
    Q_OBJECT
public slots:
//    void cppSlot(const QString &msg) {
//        qDebug() << "Called the C++ slot with message:" << msg;
//    }
};


int main(int argc, char *argv[])
{
    /* QML module import */
    qmlRegisterType<Plant>("SteamPlant",1,1, "Plant");
    qmlRegisterType<RelayBoard>("RelayBoard",1,0, "RelayBoard");
    qmlRegisterType<ADC>("ADC",1,0, "ADC");

    /* Qt app start */
    QGuiApplication app(argc, argv);

    /* QML awaking */
    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));


    /* post-QML processing */
    std::cout << "hello from c++ after QQmlApplication" << std::endl;


    return app.exec();
}
