#include <iostream>
#include <unistd.h>
#include <time.h>
#include "sjplant.h"

Plant::Plant(QQuickItem *parent) :
    QQuickItem(parent){

}

//For Heater in Boiler
    bool Plant::heaterState(){
        return _heaterState;
    }

    void Plant::setHeaterState(bool newState){
        _heaterState = newState;
        return;
    }

    float Plant::boilerPressure(){
        return _boilerPressure;
    }

    int Plant::boilerLimiter(){
        std::cout<<"boiler limiter: "<< _boilerLimiter << " kPa(G)" <<std::endl;
        return _boilerLimiter;
    }
    void Plant::setBoilerLimiter(int newValue){
        _boilerLimiter = newValue;
        return;
    }

    int Plant::swBoiler(){
        std::cout<<"_sw boiler: "<<_swBoiler<<std::endl;
        return _swBoiler;
    }

    void Plant::setSwBoiler(int newValue){
        _swBoiler = newValue;
    }

//For Suction pump
    bool Plant::pumpState(){
        return _pumpState;
    }

    void Plant::setPumpState(bool newState){
        _pumpState = newState;
        return;
    }

    float Plant::suctionPressure(){
        return _suctionPressure;
    }

    int Plant::suctionLimiter(){
        std::cout<<"suction limiter: "<< _suctionLimiter << " kPa(G)" <<std::endl;
        return _suctionLimiter;
    }

    void Plant::setSuctionLimiter(int newValue){
        _suctionLimiter = newValue;
        return;
    }

    int Plant::swPump(){
        std::cout<<"_sw pump: "<<_swPump<<std::endl;
        return _swPump;
    }

    void Plant::setSwPump(int newValue){
        _swPump = newValue;
    }

    float Plant::steamBufferPressure(){
        return _steamBufferPressure;
    }

    float Plant::vaccuumTankPressure(){
        return _vaccuumTankPressure;
    }

    // process control functions
    void Plant::autoControl(int mode){
        switch(mode){
            case 1:                                 // heating control
                if (_boilerLimiter + 2 < _boilerPressure){
                    _swBoiler = 0;
                    _flag_heated = true;
                }else{
                    _swBoiler = 1;                  // basically turn on the heater
                    if(_flag_heated){               // but when heated, turn off
                        _swBoiler = 0;
                        if(_boilerLimiter - 2 > _boilerPressure){
                            _swBoiler = 1;          // however heated, turn on
                            _flag_heated = false;   // as long as pressure is low
                        }
                    }
                }
            case 2:                                 // suction control
                if (_suctionLimiter - 2 > _suctionPressure){
                    _swPump = 0;
                    _flag_pumped = true;
                }else{
                    _swPump = 1;                         // basically turn on the pump
                    if(_flag_pumped){                    // but when pumped, turn off
                        _swPump = 0;
                        if(_suctionLimiter + 2 < _suctionPressure){
                            _swPump = 1;                 // however pumped, turn on
                            _flag_pumped = false;        // as long as pressure is not low
                        }
                    }
                }
            }
    }


    // dummy module
    void Plant::dummyModule(){
        if(_flag_dummyFirstLoad){
            std::cout<<"dummy module loaded"<<std::endl;
            _boilerPressure  = 0.0;
            _suctionPressure = 0.0;
            _flag_dummyFirstLoad = false;
        }
        if(_swBoiler == 1){
            if(_boilerPressure < _boilerLimiter +2){
                _boilerPressure += 3.7;
            }else{
                _boilerPressure -= 1.4;
                std::cout<<"boiler limiter cuts off the heating!"<<std::endl;
            }
        }
        if(_swPump == 1){
            if(_suctionPressure > _suctionLimiter -2){
                _suctionPressure -= 3.4;
            }else{
                _suctionPressure += 0.9;
                std::cout<<"suction limiter cuts off the vaccuuming!"<<std::endl;
            }
        }

        _boilerPressure  *= 0.95;
        _suctionPressure *= 0.95;

        _steamBufferPressure = _boilerPressure * 0.97;
        _vaccuumTankPressure = _suctionPressure * 0.97;

    }

//Low-water Detection Logic

    int Plant::lowWaterCheck(){

        if(_prevPressure<_boilerPressure && _heaterState){
            _counter_lowwater += 1;
        }else{
            if(_counter_lowwater>0){
                _counter_lowwater -= 1;
            }
        }
        // 5+ timesteps with pressure down & heater ON -> low-water flag turns on
        if (_counter_lowwater > 5){
            _flag_lowwater = true;
            return 0; //send back 0 (false) to QML Console
        }
        //update _prevPressure
        _prevPressure = _boilerPressure;
        return 1;
    }

    void Plant::lowWaterFlagClear(){
        _flag_lowwater = false;
    }

    void Plant::lowWaterDebug(int debugmode){
        switch(debugmode){
            case 1: // flag check
                std::cout << "Debug: _flag_lowWater is " << _flag_lowwater << std::endl;
            break;

            case 2: // flag on
                _flag_lowwater = true;
        }
    }

