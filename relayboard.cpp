#include <iostream>
#include <bitset>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <unistd.h>
#include <time.h>
#include "relayboard.h"

//Relay Board for Raspberry Pi B+
RelayBoard::RelayBoard(QQuickItem *parent) :
    QQuickItem(parent){

}


// Q_INVOLABLE RelayBoard functions

void RelayBoard::init(){
    wiringPiSetup();
    fd = wiringPiI2CSetup(dev_addr);
    if(fd<0){
        std::cerr << "I2C Error: Relay Board is not working" << std::endl;
    }
}
void RelayBoard::ch1_on(){set(1,1);}
void RelayBoard::ch2_on(){set(2,1);}
void RelayBoard::ch3_on(){set(3,1);}
void RelayBoard::ch4_on(){set(4,1);}
void RelayBoard::ch1_off(){set(1,0);}
void RelayBoard::ch2_off(){set(2,0);}
void RelayBoard::ch3_off(){set(3,0);}
void RelayBoard::ch4_off(){set(4,0);}

void RelayBoard::all_off(){
    _data = 0xff;
    // Write data
    wiringPiI2CWriteReg8(fd, mode, _data);
}

void RelayBoard::debug(){
    std::cout << "Starting debug sequence for RelayBoard..." << std::endl;
    all_off();
    usleep(100000);
    set(1, 1);
    usleep(1000000);
    set(2, 1);
    usleep(1000000);
    set(3, 1);
    usleep(1000000);
    set(4, 1);
    usleep(1000000);
    all_off();
    set(1, 1);
    set(2, 1);
    usleep(1000000);
    all_off();
    set(3, 1);
    set(4, 1);
    usleep(1000000);
    all_off();
    set(1, 1);
    set(4, 1);
    usleep(1000000);
    all_off();
    set(2, 1);
    set(3, 1);
    usleep(1000000);
    all_off();
    set(1, 1);
    set(3, 1);
    usleep(1000000);
    all_off();
    set(2, 1);
    set(4, 1);
    usleep(1000000);
    all_off();
}

// Private functions for actual data handling

void RelayBoard::set(unsigned int ch, unsigned int value){

    if(ch == 0 || ch>4){
        std::cerr << "Error: Invalid channel call for Relayboard. Call ch1 - 4." << std::endl;
        return;
    }
    if(value != 0 && value != 1){
        std::cerr << "Error: Invalid value set for Relayboard. Set 0(off)/1(on)." << std::endl;
        return;
    }
    // Data bit shift calculation
    if(value == 1){
    _data &= ~(0x1 << (ch-1));	// Turn On
    }else{
    _data |= (0x1<<(ch-1));		// Turn Off
    }

    // Write data
    wiringPiI2CWriteReg8(fd, mode, _data);
}
