#include <iostream>
#include <bitset>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <unistd.h>
#include <time.h>
#include "adc.h"

short v[4];

ADC::ADC(QQuickItem *parent) :
    QQuickItem(parent){

}

void ADC::init(){
    wiringPiSetup();
    fd = wiringPiI2CSetup(dev_addr);
    if(fd<0){
        std::cerr << "I2C Error: ADC is not working" << std::endl;
    }
    wiringPiI2CWriteReg16(fd, conf_reg, conf_init);
    usleep(100000);
}

void ADC::get(){
    int _v;
    short _vs;
    //Read the value from the I2C
    wiringPiI2CWriteReg16(fd, conf_reg, conf_ch0_ss);
    usleep(3000);
    _v = wiringPiI2CReadReg16(fd, data_reg);
    std::cout << "RAW _v[0]:      " << std::bitset<16>(_v) << std::endl;
    _v = ((_v <<8) | (_v>>8));
    std::cout << "Converted _v[0]:" << std::bitset<16>(_v) << std::endl;
    _v = _v >> 4;
    std::cout << "Shifted _v[0]:  " << std::bitset<16>(_v) << std::endl;
    _vs = (short)_v;
    std::cout << "casted _v[0]:   " << std::bitset<16>(_vs)<< std::endl;
    v[0] = _vs;

    wiringPiI2CWriteReg16(fd, conf_reg, conf_ch1_ss);
    usleep(3000);
    _v = wiringPiI2CReadReg16(fd, data_reg);
    std::cout << "RAW _v[1]:" << std::bitset<16>(_v) << std::endl;
    _v = ((_v <<8) | (_v>>8));
    std::cout << "Converted _v[1]:" << std::bitset<16>(_v) << std::endl;
    _v = _v >> 4;
    std::cout << "Shifted _v[1]:" << std::bitset<16>(_v) << std::endl;
    _vs = (short)_v;
    std::cout << "casted _v[1]:   " << std::bitset<16>(_vs)<< std::endl;
    v[1] = _vs;


    wiringPiI2CWriteReg16(fd, conf_reg, conf_ch2_ss);
    usleep(3000);
    _v = wiringPiI2CReadReg16(fd, data_reg);
    _v = ((_v <<8) | ((_v)>>8));
    _v = _v >> 4;
    std::cout << "_v[2]:" << _v << std::endl;
    v[2] = (short)_v;

    wiringPiI2CWriteReg16(fd, conf_reg, conf_ch3_ss);
    usleep(3000);
    _v = wiringPiI2CReadReg16(fd, data_reg);
    _v = ((_v <<8) | ((_v)>>8));
    _v = _v >> 4;
    std::cout << "_v[3]:" << _v << std::endl;
    v[3] = (short)_v;
}

void ADC::debug(){
    ADC::init();
    std::cout << "Starting debug sequence for ADC..." << std::endl;
    int i;
    for(i=0;i<3;i++){
        ADC::get();
        std::cout << "v[0]= " << v[0] << " [V], v[1]= " << v[1] << " [V]," <<std::endl;
        std::cout << " v[2]= " << v[2] << " [V], v[3]= " << v[3] << " [V]." <<std::endl;
        usleep(100000);
    }

}

short ADC::v0(){return v[0];}
short ADC::v1(){return v[1];}
short ADC::v2(){return v[2];}
short ADC::v3(){return v[3];}
