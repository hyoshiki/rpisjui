#ifndef RELAYBOARD_H
#define RELAYBOARD_H
#include <QQuickItem>

class RelayBoard : public QQuickItem
{

public:
    explicit RelayBoard(QQuickItem *parent = 0);
    Q_INVOKABLE void init();
    Q_INVOKABLE void ch1_on();
    Q_INVOKABLE void ch2_on();
    Q_INVOKABLE void ch3_on();
    Q_INVOKABLE void ch4_on();
    Q_INVOKABLE void ch1_off();
    Q_INVOKABLE void ch2_off();
    Q_INVOKABLE void ch3_off();
    Q_INVOKABLE void ch4_off();
    Q_INVOKABLE void all_off();
    Q_INVOKABLE void debug();

private:
    static const unsigned int dev_addr = 0x20; // Can be set by H/W resistor
    static const unsigned int mode = 0x06; // Don't care
    int fd;
    int _data;
    void set(unsigned int ch, unsigned int value);

signals:

public slots:

};

#endif // RELAYBOARD_H
