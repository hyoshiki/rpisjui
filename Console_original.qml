import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Extras 1.4
import QtQuick.Controls 1.4
//Original one
//import TestPatterns 0.2

Item {



/******** QML objects & Local variables initialization ********/
    property int testNo: 1
//    Tests{id: test_patterns}

/******** UI Components section ********/

    Switch{
        id: sw1
        width: 50; height: 40
        x:660; y:215
        style: SwitchStyle{
            groove: Rectangle{
                    implicitHeight: 60; implicitWidth: 75; radius: 15
                    color: sw1.checked? "#c2a" : "#ddd"
                    }
            handle: Rectangle{
                    implicitHeight: 60; implicitWidth: 30; radius: 15
                    color: sw1.checked ? (sw1.pressed ? "#ddd" : "#fff") : (sw1.pressed ? "#c2a" : "#fff")
                    border.color: sw1.checked ? "#c2a" : "#ddd"
                    border.width: 2
                    }
        }
    }

    Switch{
        id: sw2
        width: 50; height: 40
        x:660; y:276
        style: SwitchStyle{
            groove: Rectangle{
                    implicitHeight: 60; implicitWidth: 75; radius: 15
                    color: sw2.checked? "#c2a" : "#ddd"
                    }
            handle: Rectangle{
                    implicitHeight: 60; implicitWidth: 30; radius: 15
                    color: sw2.checked ? (sw2.pressed ? "#ddd" : "#fff") : (sw2.pressed ? "#c2a" : "#fff")
                    border.color: sw2.checked ? "#c2a" : "#ddd"
                    border.width: 2
                    }
        }
    }

    Switch{
        id: sw3
        width: 50; height: 40
        x:660; y:338
        style: SwitchStyle{
            groove: Rectangle{
                    implicitHeight: 60;
                    implicitWidth: 75; radius: 15
                    color: sw3.checked? "#c2a" : "#ddd"
                    }
            handle: Rectangle{
                    implicitHeight: 60; implicitWidth: 30; radius: 15
                    color: sw3.checked ? (sw3.pressed ? "#ddd" : "#fff") : (sw3.pressed ? "#c2a" : "#fff")
                    border.color: sw3.checked ? "#c2a" : "#ddd"
                    border.width: 2
                    }
        }
//        onCheckedChanged:{
//            switch (testNo){
//                    case 1: test_patterns.test001(5); break;
//                    case 2: test_patterns.test002(); break;
//                    case 3: test_patterns.test003(); break;
//                    case 4: test_patterns.test004(); break;
//                    case 5: test_patterns.test005(); break;
//                    case 6: test_patterns.test006(); break;
//                    case 7: test_patterns.test007(); break;
//                    case 8: test_patterns.test008(); break;
//                    case 9: test_patterns.test009(); break;
//                    case 10: test_patterns.test010(); break;
//                    case 11: test_patterns.test011(); break;
//                    case 12: test_patterns.test012(); break;
//                    case 13: test_patterns.test013(); break;
//                    case 14: test_patterns.test014(); break;
//                    case 15: test_patterns.test015(); break;
//                    case 16: test_patterns.test016(); break;
//                    case 17: test_patterns.test017(); break;
//                    case 18: test_patterns.test018(); break;
//                    case 19: test_patterns.test019(); break;
//                    default: test_patterns.invalidTestNo(testNo); break;
//                    }
//
//        }

    }

    Switch{
        id: sw4
        width: 50; height: 40
        x:660; y:402
        style: SwitchStyle{
            groove: Rectangle{
                    implicitHeight: 60; implicitWidth: 75; radius: 15
                    color: sw4.checked? "#c2a" : "#ddd"
                    }
            handle: Rectangle{
                    implicitHeight: 60; implicitWidth: 30; radius: 15
                    color: sw4.checked ? (sw4.pressed ? "#ddd" : "#fff") : (sw4.pressed ? "#c2a" : "#fff")
                    border.color: sw4.checked ? "#c2a" : "#ddd"
                    border.width: 2
            }
        }
    }


    Tumbler {
        id: tumbler1
        x:680; y:60; z: 10
        height: 120
        TumblerColumn {
            id: testNo_tumbler
            width: 80
            model: ["001", "002", "003", "004", "005", "006", "007", "008", "009", "010", "011", "012", "013", "014", "015", "016", "017", "018", "019"]
            onCurrentIndexChanged:{
                testNo = testNo_tumbler.currentIndex +1;
            }
        }
    }



/******** UI Design section ********/
    Rectangle{
        id: background_steam
        width: 320
        height: 480
        color: "#ba2636"
        x: 0
        y: 0

        Gauge {
            id: gauge1
            x: 8
            y: 95
            width: 46
            height: 367
            maximumValue: 90
            minimumValue: 0
            value: 40
        }

        ToggleButton {
            id: toggleButton2
            x: 523
            y: 347
            text: qsTr("Interlock")
        }

        Gauge {
            id: gauge2
            x: 60
            y: 95
            width: 46
            height: 367
            maximumValue: 90
            minimumValue: 0
            value: 42
        }

        Gauge {
            id: gauge3
            x: 342
            y: 95
            width: 46
            height: 367
            maximumValue: 80
            minimumValue: 0
            value: 70
        }

        Gauge {
            id: gauge4
            x: 408
            y: 95
            width: 46
            height: 367
            maximumValue: 80
            minimumValue: 0
            value: 2
        }

        Dial {
            id: dial1
            x: 200
            y: 237
            minimumValue: 10
            value: 10
            stepSize: 5
            maximumValue: 80
        }

        CircularGauge {
            id: circularGauge1
            x: 137
            y: 78
            width: 159
            height: 137
            value: 60
            stepSize: 1
            maximumValue: 90
        }

        CircularGauge {
            id: circularGauge2
            x: 460
            y: 78
            width: 159
            height: 137
            value: 50
            stepSize: 5
            maximumValue: 80
        }

        DelayButton {
            id: delayButton1
            x: 200
            y: 339
            width: 96
            height: 104
            text: qsTr("Interlock    (push hard)")
            delay: 1500
        }
    }

    Rectangle{
        id: background_suction
        width: 320
        height: 480
        color: "#3e62ad"
        z: -1
        x: 320
        y: 0
    }

    Rectangle{
        id: ready_steam
        width: 30; height: 30; radius: 15;
        border.width: 2
        color: sw1.checked ? "#0f0": "#999";
        x: 20
        y: 20
    }

    Rectangle{
        id: ready_suction
        width: 30; height: 30; radius: 15;
        border.width: 2
        color: sw2.checked ? "#0f0": "#999";
        x: 340
        y: 20
    }

    Text {
        id: state_sw1
        x: 750
        y: 224
        text: sw1.checked ? qsTr("On") : qsTr("Off")
        font.bold: true
        font.family: "Courier"
        font.pixelSize: 24
        color: sw1.checked ? "#c2a" : "#666"
    }

    Text {
        id: state_sw2
        x: 750
        y: 285
        text: sw2.checked ? qsTr("On") : qsTr("Off")
        font.family: "Courier"
        font.pixelSize: 24
        font.bold: true
        color: sw2.checked ? "#c2a" : "#666"
    }

    Text {
        id: state_sw3
        x: 750
        y: 347
        text: sw3.checked ? qsTr("On") : qsTr("Off")
        font.family: "Courier"
        font.pixelSize: 24
        font.bold: true
        color: sw3.checked ? "#c2a" : "#666"
    }

    Text {
        id: state_sw4
        x: 750
        y: 410
        text: sw4.checked ? qsTr("On") : qsTr("Off")
        font.family: "Courier"
        font.pixelSize: 24
        font.bold: true
        color: sw4.checked ? "#c2a" : "#666"
    }

    Text{
        x: 70; y:20
        text: qsTr("Steam Jet")
        font.family: "Verdana"
        font.pixelSize: 32
        font.bold: true
        color: "#ffffff"
    }

    Text {
        x: 397
        y: 20
        color: "#ffffff"
        text: qsTr("Suction")
        font.pixelSize: 32
        font.bold: true
        font.family: "Verdana"
    }

    Text {
        x: 14
        y: 75
        color: "#ffffff"
        text: qsTr("PTD1")
        font.pixelSize: 14
        font.bold: true
        font.family: "Verdana"
    }

    Text {
        x: 70
        y: 75
        color: "#ffffff"
        text: qsTr("PTD2")
        font.pixelSize: 14
        font.bold: true
        font.family: "Verdana"
    }

    Text {
        x: 348
        y: 75
        color: "#ffffff"
        text: qsTr("PTD3")
        font.pixelSize: 14
        font.bold: true
        font.family: "Verdana"
    }

    Text {
        x: 415
        y: 75
        color: "#ffffff"
        text: qsTr("PTD4")
        font.pixelSize: 14
        font.bold: true
        font.family: "Verdana"
    }

    Button {
        id: button1
        x: 680
        y: 15
        text: qsTr("Quit")
        onClicked:{
            Qt.quit();
        }
    }



}
