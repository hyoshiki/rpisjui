#include <iostream>
#include <bitset>
#include <thread>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <iostream>
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include "libi2c.h"

//Relay Board for Raspberry Pi B+
RelayBoard::RelayBoard(QQuickItem *parent) :
    QQuickItem(parent){

}

ADC::ADC(QQuickItem *parent) :
    QQuickItem(parent){

}


// RelayBoard functions

void RelayBoard::init(){
    wiringPiSetup();
    fd = wiringPiI2CSetup(dev_addr);
    if(fd<0){
        std::cerr << "I2C Error: Relay Board is not working" << std::endl;
    }
}

void RelayBoard::set(unsigned int ch, unsigned int value){

    if(ch == 0 || ch>4){
        std::cerr << "Error: Invalid channel call for Relayboard. Call ch1 - 4." << std::endl;
        return;
    }
    if(value != 0 && value != 1){
        std::cerr << "Error: Invalid value set for Relayboard. Set 0(off)/1(on)." << std::endl;
        return;
    }
    // Data bit shift calculation
    if(value == 1){
    _data &= ~(0x1 << (ch-1));	// Turn On
    }else{
    _data |= (0x1<<(ch-1));		// Turn Off
    }

    // Write data
    wiringPiI2CWriteReg8(fd, mode, _data);
}

void RelayBoard::off(){    _data = 0xff;
    // Write data
    wiringPiI2CWriteReg8(fd, mode, _data);
}


void RelayBoard::debug(){
    std::cout << "Starting debug sequence for RelayBoard..." << std::endl;
    RelayBoard::init();
    RelayBoard::off();
    usleep(100000);
    RelayBoard::set(1, 1);
    usleep(1000000);
    RelayBoard::set(2, 1);
    usleep(1000000);
    RelayBoard::set(3, 1);
    usleep(1000000);
    RelayBoard::set(4, 1);
    usleep(1000000);
    RelayBoard::off();
    RelayBoard::set(1, 1);
    RelayBoard::set(2, 1);
    usleep(1000000);
    RelayBoard::off();
    RelayBoard::set(3, 1);
    RelayBoard::set(4, 1);
    usleep(1000000);
    RelayBoard::off();
    RelayBoard::set(1, 1);
    RelayBoard::set(4, 1);
    usleep(1000000);
    RelayBoard::off();
    RelayBoard::set(2, 1);
    RelayBoard::set(3, 1);
    usleep(1000000);
    RelayBoard::set(1, 1);
    RelayBoard::set(3, 1);
    usleep(1000000);
    RelayBoard::off();
    RelayBoard::set(2, 1);
    RelayBoard::set(4, 1);
    usleep(1000000);
    RelayBoard::off();
}


// ADC functions

void ADC::init(){
    wiringPiSetup();
    fd = wiringPiI2CSetup(dev_addr);
    if(fd<0){
        std::cerr << "I2C Error: ADC is not working" << std::endl;
    }
    wiringPiI2CWriteReg16(fd, conf_reg, conf_init);
    usleep(100000);
}

void ADC::get(){
    int _v;
    short _vs;
    //Read the value from the I2C
    wiringPiI2CWriteReg16(fd, conf_reg, conf_ch0_ss);
    usleep(3000);
    _v = wiringPiI2CReadReg16(fd, data_reg);
    std::cout << "RAW _v[0]:      " << std::bitset<16>(_v) << std::endl;
    _v = ((_v <<8) | (_v>>8));
    std::cout << "Converted _v[0]:" << std::bitset<16>(_v) << std::endl;
    _v = _v >> 4;
    std::cout << "Shifted _v[0]:  " << std::bitset<16>(_v) << std::endl;
    _vs = (short)_v;
    std::cout << "casted _v[0]:   " << std::bitset<16>(_vs)<< std::endl;
    v[0] = _vs;

    wiringPiI2CWriteReg16(fd, conf_reg, conf_ch1_ss);
    usleep(3000);
    _v = wiringPiI2CReadReg16(fd, data_reg);
    std::cout << "RAW _v[1]:" << std::bitset<16>(_v) << std::endl;
    _v = ((_v <<8) | (_v>>8));
    std::cout << "Converted _v[1]:" << std::bitset<16>(_v) << std::endl;
    _v = _v >> 4;
    std::cout << "Shifted _v[1]:" << std::bitset<16>(_v) << std::endl;
    _vs = (short)_v;
    std::cout << "casted _v[1]:   " << std::bitset<16>(_vs)<< std::endl;
    v[1] = _vs;


    wiringPiI2CWriteReg16(fd, conf_reg, conf_ch2_ss);
    usleep(3000);
    _v = wiringPiI2CReadReg16(fd, data_reg);
    _v = ((_v <<8) | ((_v)>>8));
    _v = _v >> 4;
    std::cout << "_v[2]:" << _v << std::endl;
    v[2] = (short)_v;

    wiringPiI2CWriteReg16(fd, conf_reg, conf_ch3_ss);
    usleep(3000);
    _v = wiringPiI2CReadReg16(fd, data_reg);
    _v = ((_v <<8) | ((_v)>>8));
    _v = _v >> 4;
    std::cout << "_v[3]:" << _v << std::endl;
    v[3] = (short)_v;
}

void ADC::debug(){
    ADC::init();
    std::cout << "Starting debug sequence for ADC..." << std::endl;
    int i;
    for(i=0;i<3;i++){
        ADC::get();
        std::cout << "v[0]= " << v[0] << " [V], v[1]= " << v[1] << " [V]," <<std::endl;
        std::cout << " v[2]= " << v[2] << " [V], v[3]= " << v[3] << " [V]." <<std::endl;
        usleep(100000);
    }

}

//short ADC::v0(){return v[0];}
//short ADC::v1(){return v[1];}
//short ADC::v2(){return v[2];}
//short ADC::v3(){return v[3];}
