#ifndef LIBI2C_H
#define LIBI2C_H

#include <QQuickItem>

class RelayBoard : public QQuickItem
{
public:
    explicit RelayBoard(QQuickItem *parent = 0);
    Q_INVOKABLE void init();
    Q_INVOKABLE void set(unsigned int ch, unsigned int value);
    Q_INVOKABLE void off();
    Q_INVOKABLE void debug();

private:
    static const unsigned int dev_addr = 0x20; // Can be set by H/W resistor
    static const unsigned int mode = 0x06; // Don't care
    int fd;
    int _data;

signals:

public slots:

};

class ADC : public QQuickItem
{
    Q_OBJECT
//    Q_PROPERTY(short v0 READ v0)
//    Q_PROPERTY(short v1 READ v1)
//    Q_PROPERTY(short v2 READ v2)
//    Q_PROPERTY(short v3 READ v3)

public:
    explicit ADC(QQuickItem *parent = 0);
    Q_INVOKABLE void init();
    Q_INVOKABLE void get();
    Q_INVOKABLE void debug();
//    Q_INVOKABLE short v0();
//    Q_INVOKABLE short v1();
//    Q_INVOKABLE short v2();
//    Q_INVOKABLE short v3();

private:
    int fd;
    short v[4]; // 16bit integer before voltage conversion
    static const unsigned int dev_addr = 0x48;
    static const unsigned int data_reg = 0x00;    // Conversion resister: B15-B4: 12bit data, B3-B0: 0,
    static const unsigned int conf_reg = 0x01;
    static const unsigned int conf_init = 0x6383; // Config register. OS=1, MUX=000, PGA=001, MODE=1,
                                   // DR=011, CMODE=0, CPOL=0, CLAT=0, CQUE=11.0b1000001101100011.
    static const unsigned int conf_ch0_ss = 0x63c3; // FS= +/- 4.096V, Single-Shot mode
    static const unsigned int conf_ch1_ss = 0x63d3;
    static const unsigned int conf_ch2_ss = 0x63e3;
    static const unsigned int conf_ch3_ss = 0x63f3;


////  The constants below is ordered with 0x [MSB][LSB].
////  Howeveer, WiringPi 16bit R/W commands seems to transfer the data
////  with the order like 0x[LSB][MSB].
////  So, constants should be written in inverse order bytes.

//    static const unsigned int conf_init = 0x8363; // Config register. OS=1, MUX=000, PGA=001, MODE=1,
//					           // DR=011, CMODE=0, CPOL=0, CLAT=0, CQUE=11.0b1000001101100011.
//    static const unsigned int conf_ch0_ss = 0xc363; // FS= +/- 4.096V, Single-Shot mode
//    static const unsigned int conf_ch1_ss = 0xd363;
//    static const unsigned int conf_ch2_ss = 0xe363;
//    static const unsigned int conf_ch3_ss = 0xf363;
};

#endif // LIBI2C_H
