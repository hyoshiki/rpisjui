import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Extras 1.4
import SteamPlant 1.1
import RelayBoard 1.0
import ADC 1.0

Item {

    Plant{id:plant}
    RelayBoard{id:rb}
    ADC{id:adc}

    QtObject{
        id:states

        //Process staets
        property int first_trigger: 1
        //pressures
        property real pressureBoiler: Math.round(plant.boilerPressure*10)/10
        property real pressureSteamBuffer: Math.round(plant.steamBufferPressure*10)/10
        property real pressureSuction: Math.round(plant.suctionPressure*10)/10
        property real pressureVTank: Math.round(plant.vaccuumTankPressure*10)/10
        property int limiterSteamDigit10: 6
        property int limiterSteamDigit1: 0
        property int limiterSteamIntegrated: limiterSteamDigit10*10+limiterSteamDigit1
        property int limiterSuctionDigit10: 6
        property int limiterSuctionDigit1: 0
        property int limiterSuctionIntegrated: limiterSuctionDigit10*10+limiterSuctionDigit1
        property int popup_bg: -10
        property int popup_interlock: -100
        property int popup_lowWater: -100
        property int popup_manual: -100
        property int lowWater: 0
        property int interval: 3000
    }

/******** UI Base Layer Design ********/
    Rectangle{
        id: background_steam
        width: 320; height: 480
        anchors.left:parent.left; anchors.leftMargin: 0
        color: "#ba2636"
        z:1

        Text{
            id: title_steam
            x: 70; y:20
            text: qsTr("Steam")
            font.family: "Verdana"
            font.pixelSize: 32
            font.bold: true
            color: "#ffffff"
        }

        Rectangle{
            id: ready_steam
            width: 30; height: 30; radius: 15;
            border.width: 2
            color: sw1.checked ? "#0f0": "#999";
            x: 20; y: 20
        }

        Rectangle{
            id: limiter_steam
            width: 180; height:160
            radius: 10
            anchors.left:parent.left; anchors.leftMargin: 20
            anchors.top:title_steam.bottom; anchors.topMargin: 10
            color:"#33000000"
            Text {
                anchors.left:parent.left; anchors.leftMargin: 5
                anchors.top:parent.top; anchors.topMargin: 5
                color: "white"
                text: qsTr("Boiler Pressure Limit")
                font.pixelSize: 12
                font.bold: true
                font.family: "Verdana"
            }
            Text {
                anchors.right:parent.right; anchors.rightMargin: 5
                anchors.bottom:parent.bottom; anchors.bottomMargin: 35
                color: "white"
                text: qsTr("kPa(G)")
                font.pixelSize: 12
                font.bold: true
                font.family: "Verdana"
            }
            Button{
                id:limiter_steam_plus10
                anchors.left:parent.left; anchors.leftMargin: 10
                anchors.top:parent.top; anchors.topMargin: 25
                style:ButtonStyle{
                    background: Rectangle{
                        implicitWidth: 50;implicitHeight: 30
                        radius:5
                        color: control.pressed ? "#e4c":"#555"
                    }
                }
                onClicked: {
                    states.limiterSteamDigit10 += 1;
                    if(states.limiterSteamDigit10>9){
                        states.limiterSteamDigit10 = 9;
                    }
                }
                Image{
                    anchors.top: parent.top; anchors.topMargin: 2
                    source: "arrow.png"
                }
            }
            Button{
                id:limiter_steam_minus10
                anchors.left:limiter_steam_plus10.left
                anchors.bottom:parent.bottom; anchors.bottomMargin: 10
                style:ButtonStyle{
                    background: Rectangle{
                        implicitWidth: 50;implicitHeight: 30
                        radius:5
                        color: control.pressed ? "#e4c":"#555"
                    }
                }
                onClicked: {
                    states.limiterSteamDigit10 -= 1;
                    if(states.limiterSteamDigit10<0){
                        states.limiterSteamDigit10 = 0;
                    }
                }
                Image{
                    anchors.top: parent.top; anchors.topMargin: -2
                    source: "arrow.png"
                    rotation: 180
                }
            }
            Button{
                id:limiter_steam_plus1
                anchors.left:limiter_steam_plus10.right; anchors.leftMargin: 3
                anchors.top:limiter_steam_plus10.top
                style:ButtonStyle{
                    background: Rectangle{
                        implicitWidth: 50;implicitHeight: 30
                        radius:5
                        color: control.pressed ? "#e4c":"#555"
                    }
                }
                onClicked: {
                    states.limiterSteamDigit1 += 1;
                    if(states.limiterSteamDigit1>9){
                        states.limiterSteamDigit1 = 0;
                        if(states.limiterSteamDigit10==9){
                            states.limiterSteamDigit1 = 9;
                        }else{
                            states.limiterSteamDigit10 += 1;
                        }
                    }
                }
                Image{
                    anchors.top: parent.top; anchors.topMargin: 2
                    source: "arrow.png"
                }
            }
            Button{
                id:limiter_steam_minus1
                anchors.left:limiter_steam_minus10.right; anchors.leftMargin: 3
                anchors.top:limiter_steam_minus10.top
                style:ButtonStyle{
                    background: Rectangle{
                        implicitWidth: 50;implicitHeight: 30
                        radius:5
                        color: control.pressed ? "#e4c":"#555"
                    }
                }
                onClicked: {
                    states.limiterSteamDigit1 -= 1;
                    if(states.limiterSteamDigit1<0){
                        states.limiterSteamDigit1 = 9;
                        if(states.limiterSteamDigit10 ==0){
                            states.limiterSteamDigit1 = 0;
                        }else{
                            states.limiterSteamDigit10 -= 1;
                        }
                    }
                }
                Image{
                    anchors.top: parent.top; anchors.topMargin: -2
                    source: "arrow.png"
                    rotation: 180
                }
            }
            Rectangle{
                id: limiter_steam_indicator
                anchors.top: limiter_steam_plus10.bottom
                anchors.bottom: limiter_steam_minus10.top
                anchors.left: limiter_steam_plus10.left
                anchors.right: limiter_steam_minus1.right
                color:"black"
            }
            Text {
                id: limiter_steam_digit10
                anchors.top: limiter_steam_plus10.bottom
                anchors.bottom: limiter_steam_minus10.top
                anchors.horizontalCenter: limiter_steam_plus10.horizontalCenter
                color: "#33ff33"
                text: states.limiterSteamDigit10
                font.pixelSize: 56
                font.bold: true
                font.family: "Verdana"
            }
            Text {
                id: limiter_steam_digit1
                anchors.top: limiter_steam_plus1.bottom
                anchors.bottom: limiter_steam_minus1.top
                anchors.horizontalCenter: limiter_steam_plus1.horizontalCenter
                color: "#33ff33"
                text: states.limiterSteamDigit1
                font.pixelSize: 56
                font.bold: true
                font.family: "Verdana"
            }
        }


        Rectangle{
            id: indicator1
            width: 280; height: 115;
            radius:15
            anchors.left: parent.left; anchors.leftMargin: 20
            anchors.right: parent.right; anchors.rightMargin: 20
            anchors.bottom: indicator2.top; anchors.bottomMargin: 5
            color:"#33000000"
            Text {
                x: 10; y: 7; z: 3
                color: "white"
                text: qsTr("PTD1")
                font.pixelSize: 14
                font.bold: true
                font.family: "Verdana"
            }
            Text {
                anchors.bottom: parent.bottom; anchors.bottomMargin: 10
                anchors.right: parent.right; anchors.rightMargin: 5
                color: "white"
                text: qsTr("kPa(G)")
                font.pixelSize: 16
                font.bold: true
                font.family: "Verdana"
            }
            Rectangle{
                width: 200; height: 75
                x: 10; y: 30; z: 3
                color: "black"
                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right; anchors.rightMargin: 10
                    color: "#33ff33"
                    text: states.pressureBoiler
                    font.pixelSize: 56
                    font.bold: true
                    font.family: "Verdana"
                }
            }

        }//indicator1

        Rectangle{
            id: indicator2
            height: 115; radius:15
            anchors.left: parent.left; anchors.leftMargin: 20
            anchors.right: parent.right; anchors.rightMargin: 20
            anchors.bottom: parent.bottom; anchors.bottomMargin: 10
            color:"#33000000"
            Text {
                x: 10; y: 7; z: 3
                color: "white"
                text: qsTr("PTD2")
                font.pixelSize: 14
                font.bold: true
                font.family: "Verdana"
            }
            Text {
                anchors.bottom: parent.bottom; anchors.bottomMargin: 10
                anchors.right: parent.right; anchors.rightMargin: 5
                color: "white"
                text: qsTr("kPa(G)")
                font.pixelSize: 16
                font.bold: true
                font.family: "Verdana"
            }
            Rectangle{
                width: 200; height: 75
                x: 10; y: 30; z: 3
                color: "black"
                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right; anchors.rightMargin: 10
                    color: "#33ff33"
                    text: states.pressureSteamBuffer
                    font.pixelSize: 56
                    font.bold: true
                    font.family: "Verdana"
                }
            }

        }//indicator2

    }//background steam

    Rectangle{
        id: background_suction
        width: 330; height: 480
        anchors.left:background_steam.right; anchors.leftMargin: 0
        color: "#3e62ad"
        z:1;

        Text {
            id: title_suction
            x: 70; y: 20
            color: "#ffffff"
            text: qsTr("Suction")
            font.pixelSize: 32
            font.bold: true
            font.family: "Verdana"
        }

        Rectangle{
            id: ready_suction
            width: 30; height: 30; radius: 15;
            border.width: 2
            color: sw1.checked ? "#0f0": "#999";
            x: 20; y: 20
        }

        Rectangle{
            id: limiter_suction
            width: 200; height:160
            radius: 10
            anchors.left:parent.left; anchors.leftMargin: 20
            anchors.top:title_suction.bottom; anchors.topMargin: 10
            color:"#33000000"
            Text {
                anchors.left:parent.left; anchors.leftMargin: 5
                anchors.top:parent.top; anchors.topMargin: 5
                color: "white"
                text: qsTr("Suction Pressure Limit")
                font.pixelSize: 12
                font.bold: true
                font.family: "Verdana"
            }
            Text {
                anchors.right:parent.right; anchors.rightMargin: 5
                anchors.bottom:parent.bottom; anchors.bottomMargin: 35
                color: "white"
                text: qsTr("kPa(G)")
                font.pixelSize: 12
                font.bold: true
                font.family: "Verdana"
            }
            Button{
                id:limiter_suction_plus10
                anchors.left:parent.left; anchors.leftMargin: 35
                anchors.top:parent.top; anchors.topMargin: 25
                style:ButtonStyle{
                    background: Rectangle{
                        implicitWidth: 50;implicitHeight: 30
                        radius:5
                        color: control.pressed ? "#e4c":"#555"
                    }
                }
                onClicked: {
                    states.limiterSuctionDigit10 += 1;
                    if(states.limiterSuctionDigit10>9){
                        states.limiterSuctionDigit10 = 9;
                    }

                }
                Image{
                    anchors.top: parent.top; anchors.topMargin: 2
                    source: "arrow.png"
                }
            }
            Button{
                id:limiter_suction_minus10
                anchors.left:limiter_suction_plus10.left
                anchors.bottom:parent.bottom; anchors.bottomMargin: 10
                style:ButtonStyle{
                    background: Rectangle{
                        implicitWidth: 50;implicitHeight: 30
                        radius:5
                        color: control.pressed ? "#e4c":"#555"
                    }
                }
                onClicked: {
                    states.limiterSuctionDigit10 -= 1;
                    if(states.limiterSuctionDigit10<0){
                        states.limiterSuctionDigit10 = 0;
                    }
                }
                Image{
                    anchors.top: parent.top; anchors.topMargin: -2
                    source: "arrow.png"
                    rotation: 180
                }
            }
            Button{
                id:limiter_suction_plus1
                anchors.left:limiter_suction_plus10.right; anchors.leftMargin: 3
                anchors.top:limiter_suction_plus10.top
                style:ButtonStyle{
                    background: Rectangle{
                        implicitWidth: 50;implicitHeight: 30
                        radius:5
                        color: control.pressed ? "#e4c":"#555"
                    }
                }
                onClicked: {
                    states.limiterSuctionDigit1 += 1;
                    if(states.limiterSuctionDigit1>9){
                        states.limiterSuctionDigit1 = 0;
                        if(states.limiterSuctionDigit10==9){
                            states.limiterSuctionDigit1 = 9;
                        }else{
                            states.limiterSteamDigit10 += 1;
                        }
                    }
                }
                Image{
                    anchors.top: parent.top; anchors.topMargin: 2
                    source: "arrow.png"
                }
            }
            Button{
                id:limiter_suction_minus1
                anchors.left:limiter_suction_minus10.right; anchors.leftMargin: 3
                anchors.top:limiter_suction_minus10.top
                style:ButtonStyle{
                    background: Rectangle{
                        implicitWidth: 50;implicitHeight: 30
                        radius:5
                        color: control.pressed ? "#e4c":"#555"
                    }
                }
                onClicked: {
                    states.limiterSuctionDigit1 -= 1;
                    if(states.limiterSuctionDigit1<0){
                        states.limiterSuctionDigit1 = 9;
                        if(states.limiterSuctionDigit10==0){
                            states.limiterSuctionDigit1 = 0;
                        }else{
                            states.limiterSuctionDigit10 -= 1 ;
                        }
                    }
                }
                Image{
                    anchors.top: parent.top; anchors.topMargin: -2
                    source: "arrow.png"
                    rotation: 180
                }
            }
            Rectangle{
                id: limiter_suction_indicator
                anchors.top: limiter_suction_plus10.bottom
                anchors.bottom: limiter_suction_minus10.top
                anchors.left: limiter_suction_plus10.left; anchors.leftMargin:-25
                anchors.right: limiter_suction_minus1.right
                color:"black"
            }
            Text {
                id: limiter_suction_digit10
                anchors.top: limiter_suction_plus10.bottom
                anchors.bottom: limiter_suction_minus10.top
                anchors.horizontalCenter: limiter_suction_plus10.horizontalCenter
                color: "#33ff33"
                text: states.limiterSuctionDigit10
                font.pixelSize: 56
                font.bold: true
                font.family: "Verdana"
            }
            Text {
                id: limiter_suction_digit1
                anchors.top: limiter_suction_plus1.bottom
                anchors.bottom: limiter_suction_minus1.top
                anchors.horizontalCenter: limiter_suction_plus1.horizontalCenter
                color: "#33ff33"
                text: states.limiterSuctionDigit1
                font.pixelSize: 56
                font.bold: true
                font.family: "Verdana"
            }
            Text {
                id: limiter_suction_digitminus
                anchors.top: limiter_suction_digit1.top
                anchors.bottom: limiter_suction_digit1.bottom
                anchors.right: limiter_suction_digit10.left; anchors.rightMargin: 7
                color: "#33ff33"
                text: qsTr("-")
                font.pixelSize: 56
                font.bold: true
                font.family: "Verdana"
            }
        }//limiter_suction


        Rectangle{
            id: indicator3
            height: 115; radius:15
            anchors.left: parent.left; anchors.leftMargin: 20
            anchors.right: parent.right; anchors.rightMargin: 20
            anchors.bottom: indicator4.top; anchors.bottomMargin: 5
            color:"#33000000"
            Text {
                x: 10; y: 7; z: 3
                color: "white"
                text: qsTr("PTD3")
                font.pixelSize: 14
                font.bold: true
                font.family: "Verdana"
            }
            Text {
                anchors.bottom: parent.bottom; anchors.bottomMargin: 10
                anchors.right: parent.right; anchors.rightMargin: 5
                color: "white"
                text: qsTr("kPa(G)")
                font.pixelSize: 16
                font.bold: true
                font.family: "Verdana"
            }
            Rectangle{
                width: 200; height: 75
                x: 10; y: 30; z: 3
                color: "black"
                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right; anchors.rightMargin: 10
                    color: "#33ff33"

                    text: states.pressureSuction
                    font.pixelSize: 56
                    font.bold: true
                    font.family: "Verdana"
                }
            }

        }//indicator3

        Rectangle{
            id: indicator4
            height: 115; radius: 15
            anchors.left: parent.left; anchors.leftMargin: 20
            anchors.right: parent.right; anchors.rightMargin: 20
            anchors.bottom: parent.bottom; anchors.bottomMargin: 10
            color:"#33000000"
            Text {
                x: 10; y: 7; z: 3
                color: "white"
                text: qsTr("PTD4")
                font.pixelSize: 14
                font.bold: true
                font.family: "Verdana"
            }
            Text {
                anchors.bottom: parent.bottom; anchors.bottomMargin: 10
                anchors.right: parent.right; anchors.rightMargin: 5
                color: "white"
                text: qsTr("kPa(G)")
                font.pixelSize: 16
                font.bold: true
                font.family: "Verdana"
            }
            Rectangle{
                width: 200; height: 75
                x: 10; y: 30; z: 3
                color: "black"
                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right; anchors.rightMargin: 10
                    color: "#33ff33"
                    text: states.pressureVTank
                    font.pixelSize: 56
                    font.bold: true
                    font.family: "Verdana"
                }
            }

        }//indicator4

    }//background suction

    Rectangle{
        id: background_switches
        width:150; height:480
        anchors.left: background_suction.right
        anchors.leftMargin: 0
        z:-1
        color:"#333"

        Text{
            id:title_switches
//            anchors.right:parent.right; anchors.leftMargin: 5
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top; anchors.topMargin: 10
            font.pointSize: 14
//            font.bold: true
            color: "white"
            text:qsTr("Switches")
        }

        /******** Switches ********/

        Switch{
            id: sw1
            width: 75; height: 75
            anchors.left: parent.left; anchors.leftMargin: 12
            anchors.top: title_switches.bottom; anchors.topMargin: 40
            style: SwitchStyle{
                groove: Rectangle{
                        implicitHeight: 75; implicitWidth: 75; radius: 15
                        color: sw1.checked? "#999" : "#e4c"
                        }
                handle: Rectangle{
                        implicitHeight: 75; implicitWidth: 30; radius: 15
                        color: sw1.checked ? (sw1.pressed ? "#999" : "#fff") : (sw1.pressed ? "#e4c" : "#fff")
                        border.color: sw1.checked ? "#999" : "#e4c"
                        border.width: 2
                        }
            }
            onCheckedChanged: {
                if(this.checked){
                rb.ch3_on;
                states.interval = 750;
                background_popup.z = 10;
                popup_manualMode.z = 100;
                }else{
                rb.all_off;
                plant.setSwBoiler(0);                   
                plant.setSwPump(0);
                states.interval = 3000;
                }
            }
        }

        Switch{
            id: sw2
            width: 75; height: 45
            anchors.left: sw1.left
            anchors.top: sw1.bottom; anchors.topMargin: 25
            style: SwitchStyle{
                groove: Rectangle{
                        implicitHeight: 45; implicitWidth: 75; radius: 15
                        color: sw2.checked? "#e4c" : "#999"
                        }
                handle: Rectangle{
                        implicitHeight: 45; implicitWidth: 30; radius: 15
                        color: sw2.checked ? (sw2.pressed ? "#999" : "#fff") : (sw2.pressed ? "#e4c" : "#fff")
                        border.color: sw2.checked ? "#e4c" : "#999"
                        border.width: 2
                        }
                }
            onCheckedChanged:{
                if(sw2.checked){
                    console.log('Hello from sw2')
                    //TODO: implement Automatic control mode
                }
            }
        }

        Switch{
            id: sw3
            width: 75; height: 45
            anchors.left: sw2.left
            anchors.top: sw2.bottom; anchors.topMargin: 25
            style: SwitchStyle{
                groove: Rectangle{
                        implicitHeight: 45;
                        implicitWidth: 75; radius: 15
                        color: sw3.checked? "#e4c" : "#999"
                        }
                handle: Rectangle{
                        implicitHeight: 45; implicitWidth: 30; radius: 15
                        color: sw3.checked ? (sw3.pressed ? "#999" : "#fff") : (sw3.pressed ? "#e4c" : "#fff")
                        border.color: sw3.checked ? "#e4c" : "#999"
                        border.width: 2
                        }
            }
            onCheckedChanged: {
                console.log('Hello from sw3');
                //TODO: implement no suction mode
            }
        }

        Switch{
            id: sw4
            width: 75; height: 45
            anchors.left: sw3.left
            anchors.top: sw3.bottom; anchors.topMargin: 25
            style: SwitchStyle{
                groove: Rectangle{
                        implicitHeight: 45; implicitWidth: 75; radius: 15
                        color: sw4.checked? "#e4c" : "#999"
                        }
                handle: Rectangle{
                        implicitHeight: 45; implicitWidth: 30; radius: 15
                        color: sw4.checked ? (sw4.pressed ? "#999" : "#fff") : (sw4.pressed ? "#e4c" : "#fff")
                        border.color: sw4.checked ? "#e4c" : "#999"
                        border.width: 2
                }
            }
            onCheckedChanged: {
                if(sw4.checked){
                    console.log('Hi from sw4');
                    //TODO: implement manual mode winddow popup
                    if(this.checked){
                    states.interval = 1000;
                    background_popup.z = 10;
                    popup_manualMode.z = 100;
                    console.log('pop!')
                    }else{
                    states.interval = 3000;
                        background_popup.z = -10;
                        popup_manualMode.z = -100;
                    }
                }
            }
        }

        Text {
            id: label_sw1
            anchors.bottom: sw1.top; anchors.bottomMargin: 7
            anchors.left: sw1.left; anchors.leftMargin: 0
            text: qsTr("INTERLOCK")
            font.bold: true
//            font.family: "Courier"
            font.pixelSize: 16
            color: sw1.checked ? "#666" : "#e4c"
        }

        Text {
            id: state_sw1
            anchors.verticalCenter: sw1.verticalCenter
            anchors.left: sw1.right; anchors.leftMargin: 5
            text: sw1.checked ? qsTr("Active") : qsTr("Locked")
            font.bold: true
            font.family: "Courier"
            font.pixelSize: sw1.checked ? 14 : 16
            color: sw1.checked ? "#666" : "#e4c"
        }

        Text {
            id: label_sw2
            anchors.bottom: sw2.top; anchors.bottomMargin: 0
            anchors.left: sw2.left; anchors.leftMargin: 0
            text: qsTr("Automatic mode")
//            font.bold: true
//            font.family: "Courier"
            font.pixelSize: 14
            color: sw2.checked ? "#e4c" : "#fff"
        }
        Text {
            id: state_sw2
            anchors.verticalCenter: sw2.verticalCenter
            anchors.left: sw2.right; anchors.leftMargin: 5
            text: sw2.checked ? qsTr("ON") : qsTr("Off")
            font.family: "Courier"
            font.pixelSize: sw2.checked ? 32 : 24
            font.bold: true
            color: sw2.checked ? "#e4c" : "#666"
        }

        Text {
            id: label_sw3
            anchors.bottom: sw3.top; anchors.bottomMargin: 0
            anchors.left: sw3.left; anchors.leftMargin: 0
            text: qsTr("No Suction mode")
//           font.bold: true
//            font.family: "Courier"
            font.pixelSize: 14
            color: sw3.checked ? "#e4c" : "#fff"
        }
        Text {
            id: state_sw3
            anchors.verticalCenter: sw3.verticalCenter
            anchors.left: sw3.right; anchors.leftMargin: 5
            text: sw3.checked ? qsTr("ON") : qsTr("Off")
            font.family: "Courier"
            font.pixelSize: sw3.checked ? 32 : 24
            font.bold: true
            color: sw3.checked ? "#e4c" : "#666"
        }

        Text {
            id: label_sw4
            anchors.bottom: sw4.top; anchors.bottomMargin: 0
            anchors.left: sw4.left; anchors.leftMargin: 0
            text: qsTr("Manual mode")
//            font.bold: true
//            font.family: "Courier"
            font.pixelSize: 14
            color: sw4.checked ? "#e4c" : "#fff"
        }
        Text {
            id: state_sw4
            anchors.verticalCenter: sw4.verticalCenter
            anchors.left: sw4.right; anchors.leftMargin: 5
            text: sw4.checked ? qsTr("ON") : qsTr("Off")
            font.family: "Courier"
            font.pixelSize: sw4.checked ? 32 : 24
            font.bold: true
            color: sw4.checked ? "#e4c" : "#666"
        }


        Button {
            id: quit_btn
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom; anchors.bottomMargin: 10
            text: qsTr("Quit")
            onClicked:{
                Qt.quit();
            }
        }

    }//background_switches

/******** Popup Control Panel Layer ********/

    Rectangle{
        id: background_popup
        color:"#66000000"
        x:0;y:0;z:states.popup_bg
        width:800; height:480
        Button {
            id: btn_popupClose
            width:200; height:60
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom; anchors.bottomMargin: 15
            text: qsTr("Close this popup window")
            onClicked:{
                states.popup_bg = -10;
                states.popup_interlock = -100;
                states.popup_lowWater = -100;
                states.popup_manualMode = -100;
            }
        }
        Rectangle{
            id: popup_interlockRelease
            width:540; height: 360
            anchors.top: parent.top; anchors.topMargin: 35
            anchors.horizontalCenter: parent.horizontalCenter
            z: states.popup_interlock
            color:"#eeeeee"
            Text{
                id: warning_interlockRelease
                anchors.verticalCenter: parent.verticalCenter
                anchors.verticalCenterOffset: -30
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                text: qsTr("Interlock is released.\nBe careful for handling the device.")
                color:"#f33"
                font.bold: true
                font.pointSize: 20
                Timer{
                    id: timer_popup_interlockRelease
                    interval: 2000; running:sw1.checked; repeat:false;
                    onTriggered: {
                        parent.z = -100
                        background_popup.z = -10
                    }
                }
            }

        }
        Rectangle{
            id: popup_lowWaterAlert
            width:540; height: 360
            anchors.top: parent.top; anchors.topMargin: 35
            anchors.horizontalCenter: parent.horizontalCenter
            z: states.popup_lowWater
            color:"#eeeeee"
            Text{
                id: warning_lowWater
                x: 20; y:20
                anchors.fill: parent
                text: qsTr("Warning! Low-water detected.")
                color:"#f33"
                font.pointSize: 16
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignHCenter
            }
            Button{
                id: btn_popupClose_lowWater
                width: 150; height: 60
                anchors.bottom: parent.bottom; anchors.bottomMargin: 20
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.horizontalCenterOffset: -100
                text: qsTr("Close this window")
                onClicked: {
                    parent.z = -100;
                    background_popup.z = -10;
                    plant.lowWaterDebug(1);
                }
            }
            Button{
                id: btn_popup_lowWaterFlagClear
                width: 150; height: 60
                anchors.bottom: parent.bottom; anchors.bottomMargin: 20
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.horizontalCenterOffset: 100
                text: qsTr("Clear _flag_lowWater")
                onClicked: {plant.lowWaterFlagClear();}
            }
        }

        Rectangle{
            id: popup_manualMode
            width:540; height: 360
            anchors.top: parent.top; anchors.topMargin: 35
            anchors.horizontalCenter: parent.horizontalCenter
            z: states.popup_manual
            color:"#eeeeee"
            Text{
                id: title_manual
                width: 200; height: 30
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Manual mode")
                color:"#f33"
                font.pointSize: 16
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignHCenter
            }
            Button{
                id: btn_heater_on
                width: 80; height: 30
                anchors.top: parent.top; anchors.topMargin: 40
                anchors.left: parent.left; anchors.leftMargin: 10
                text: qsTr("heater: ON")
                onClicked: {
                    plant.setSwBoiler(1);
                    rb.ch1_on;
                }
            }
            Button{
                id: btn_heater_off
                width: 80; height: 30
                anchors.top: btn_heater_on.top
                anchors.left: btn_heater_on.right; anchors.leftMargin: 10
                text: qsTr("heater: off")
                onClicked: {
                    plant.setSwBoiler(0);
                    rb.ch1_off;
                }
            }
            Button{
                id: btn_pump_on
                width: 80; height: 30
                anchors.top: btn_heater_on.bottom; anchors.topMargin: 10
                anchors.left: parent.left; anchors.leftMargin: 10
                text: qsTr("pump: ON")
                onClicked: {
                    plant.setSwPump(1);
                    rb.ch2_on;
                }
            }
            Button{
                id: btn_pump_off
                width: 80; height: 30
                anchors.top: btn_pump_on.top
                anchors.left: btn_pump_on.right; anchors.leftMargin: 10
                text: qsTr("pump: off")
                onClicked: {
                    plant.setSwPump(0);
                    rb.ch2_off;
                }
            }
            Button{
                id: btn_debug_lowwater
                width: 80; height: 30
                anchors.top: parent.top; anchors.topMargin: 10
                anchors.left: parent.left; anchors.leftMargin: 120
                text:qsTr("Debug: lwd logic")
                onClicked: {
                    //Low-water detection logic debugging
                    popup_lowWaterAlert.z = 102;
                    plant.lowWaterDebug(2);
                }
            }
            Button{
                id: btn_debug_rb
                width: 80; height: 30
                anchors.top: btn_debug_lowwater.bottom; anchors.topMargin: 10
                anchors.left: btn_debug_lowwater.left
                text:qsTr("Debug: rb")
                onClicked: {
                    rb.debug;
                }
            }
            Button{
                id: btn_debug_adc
                width: 80; height: 30
                anchors.top: btn_debug_rb.bottom; anchors.topMargin: 10
                anchors.left: btn_debug_rb.left
                text:qsTr("Debug: adc")
                onClicked: {
                    adc.debug;
                }
            }
        }
    }


/******** Processes ********/

    Timer {
        id: timer_processes
        interval: states.interval; running:true; repeat:true;

        onIntervalChanged: {
            timer_processes.triggeredOnStart
        }
        onTriggered: {
            if(states.first_trigger){
                rb.init;
                adc.init;
                rb.ch1_on;
                states.first_trigger = 0;
                console("First initialization is done.");
            }
//        // Load dummy
//            plant.dummyModule();

          // Update values from hardware
            adc.get;
            console.log(adc.v0)
            console.log(adc.v1)
            console.log(adc.v2)
            console.log(adc.v3)

          // Update gauges with values rounded to one decimal place
            states.pressureBoiler      = Math.round((((adc.v0()*2)-1000)*0.125)*10)/10;     // P[kPa]= 0.125*(mV-1000)
            states.pressureSteamBuffer = Math.round((((adc.v1()*2)-1000)*0.125)*10)/10;
            states.pressureVTank       = (Math.round((((adc.v2()*2)-1000)*0.075)*10)-1)/10; // P[kPa]= 0.075*(mV-1000) - 0.1
            states.pressureSuction     = (Math.round((((adc.v3()*2)-1000)*0.075)*10)-1)/10;

        // Send values of States
            states.limiterSteamIntegrated = states.limiterSteamDigit10*10+states.limiterSteamDigit1;
            states.limiterSuctionIntegrated = (-1)*(states.limiterSuctionDigit10*10+states.limiterSuctionDigit1);
            plant.setBoilerLimiter(states.limiterSteamIntegrated);
            plant.setSuctionLimiter(states.limiterSuctionIntegrated);

        // Low-water Detection
            if(!plant.lowWaterCheck){
                states.lowWater = 1;
            }


        //debug msgs
            plant.boilerLimiter;
            plant.suctionLimiter;
        }
    }

}
